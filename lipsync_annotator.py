import argparse
import os
import sys
import time
import pdb
import argparse
import pickle
import glob
import shutil
import itertools

import random
from natsort import natsorted
import pandas as pd
import numpy as np
import json

# Lipsync Multi Face Datamaker 
# python lipsync_annotator.py --name project_folder/video_folder --download_dir video_folder
# Enter Menu driven mode

class Lipsync_datamaker:

    def __init__(self, args, json_dict, video_dict):
        super(Lipsync_datamaker, self).__init__()

        self.json_dict = json_dict
        self.json_path = args.name + '/' + args.name.split('/')[-1] + '.json'
        self.video_dict = video_dict
        self.video_json_path = ""
        self.cleaned_json_path = ""
        self.cleaned_json = []
        self.sample_dict, self.frame_dict = {},{}
        self.video_id = ""

        self.project_dir = args.name
        self.base_dir = args.download_dir
        self.sample_dir = args.sample_dir
        self.cleaned_dir = args.cleaned_dir
        self.max_frames = args.max_frames
        self.sample_id, self.sample_st, self.sample_end = 0, 0, self.max_frames
        self.new_audio, self.new_video = "",""

        self.video_fps = args.v_fps
        self.sampleRate = args.a_fps
        self.list_file = open(args.result_list + '_' + str(self.max_frames) + '_'  + str(args.phase) + '.txt', 'a')
        self.thread = args.num_thread
        self.frame_list = []


    def save_json(self):
        with open(self.json_path, 'w') as fp:
            json.dump(self.json_dict, fp)
        
        with open(self.video_json_path, 'w') as fp:
            json.dump(self.video_dict, fp)
            
    def update_json(self):
        with open(self.cleaned_json_path, 'w') as fp:
            json.dump(self.cleaned_json, fp)
    
    def clean_samples(self):

        cleaned_dir = self.cleaned_dir
        os.makedirs(cleaned_dir,exist_ok=True)
        videos = os.listdir(self.sample_dir)
        videos = natsorted(videos)
        for video in videos:

            print("\n\nCleaning video %s\n"%(video))
            isCleaned = 1
            
            # vid_id = str(os.path.splitext(video.split('/')[-1])[0])
            vid_id = video.split('/')[-1] 
            print(vid_id)
            vid_dir = os.path.join(self.sample_dir,vid_id)
            json_file = open(os.path.join(vid_dir,vid_id +'.json'),'r') 
            vid_json = json.load(json_file)
            
            clean_vid_dir = os.path.join(cleaned_dir, vid_id) 
            os.makedirs(clean_vid_dir,exist_ok=True)
            self.cleaned_json_path = os.path.join(clean_vid_dir,vid_id + '.json')
            if os.path.exists(self.cleaned_json_path):
                clean_json = open(self.cleaned_json_path,'r')
                self.cleaned_json = json.load(clean_json)
            else:
                self.cleaned_json = vid_json
            
            option = 'y'
            if self.cleaned_json['isCleaned'] == 1:
                option = 'n'
                print(("\nExiting! Already Cleaned this video: %s"%(video)))

            if option == 'y':
                for i,scene in enumerate(vid_json['scene']):
                    for j,sample in enumerate(scene['sample']):
                        sample_path = os.path.join(vid_dir,'samples',vid_id + '_sample_' + str(sample['sample_id']))
                        clean_sample_dir = os.path.join(clean_vid_dir,vid_id + '_sample_' + str(sample['sample_id']))
                        os.makedirs(clean_sample_dir,exist_ok=True)
                        annotate_file =  os.path.join(sample_path,vid_id + '_sample_' + str(sample['sample_id']) + '_annotate.avi')
                        sampleoption = 'y'
                        
                        if self.cleaned_json['scene'][i]['sample'][j]['isCleaned'] == 1:
                            sampleoption = 'n'
                        
                        if sampleoption == 'y':

                            print("Provide inSync / Speaker ID in this video => ",annotate_file)
                            isSync = int(input("\nIs the video inSync? [0 - OffSync / 1- InSync]: "))

                            self.cleaned_json['scene'][i]['sample'][j]['isSync'] = isSync

                            if isSync:
                                    speaker_id = int(input("\nSpeaker ID? Your input [0-N] : "))
                                    print("\nMarking Speaker %d as talking head\n"%(speaker_id))
                                    self.cleaned_json['scene'][i]['sample'][j]['TalkID'] = speaker_id
                                    
                                    self.cleaned_json['scene'][i]['sample'][j]['isCleaned'] = 1
                                    self.update_json()
                                    sample_vid = vid_id + '_sample_' + str(sample['sample_id']) + '_face_' + str(speaker_id) + '.avi'
                                    speaker_sample = os.path.join(sample_path ,sample_vid )
                                    sample_aud = vid_id + '_sample_' + str(sample['sample_id']) + '.wav'
                                    speaker_audio = os.path.join(sample_path,sample_aud)
                                    shutil.copy(speaker_sample,clean_sample_dir)
                                    shutil.copy(speaker_audio,clean_sample_dir)

                                    self.list_file.write('%s %s %d\n'%(os.path.join(clean_sample_dir,sample_vid), os.path.join(clean_sample_dir,sample_aud), self.max_frames))

                            else:
                                self.cleaned_json['scene'][i]['sample'][j]['isCleaned'] = 1
                                print("\nMarking Sample as %d [0 - OffSync / 1- InSync] "%(isSync))
                                shutil.rmtree(clean_sample_dir)
                                self.update_json()

            self.cleaned_json['isCleaned'] = 1
            self.update_json()

if __name__ == '__main__':

    parser=argparse.ArgumentParser(description = "LipSync Datamaker")

    parser.add_argument(
        '--name', type = str, default = 'LipSync_Face_Data_V3/test/', help = 'Give name for Project',required=True)
    parser.add_argument('--download_dir', type = str, default = 'lipsync_download_data',
                        help = 'Base Dir to keep downloaded videos')
    parser.add_argument('--sample_dir', type = str, default = 'lipsync_samples',
                        help = 'Target Dir to keep packed (face/mouth, audio, csv)')
    parser.add_argument('--cleaned_dir', type = str, default = 'lipsync_cleaned_samples',
                        help = 'Target Dir to keep cleaned samples (face/mouth, audio)')
    parser.add_argument('--max_frames', type = int,
                        default = 50, help = 'Max frames for a video pair')
    parser.add_argument('--v_fps', type = int,
                        default = 25, help = 'Video FPS')
    parser.add_argument('--a_fps', type = int,
                        default = 16000, help = 'Audio Sample rate')
    parser.add_argument('--phase', type = str,
                        default = 'train', help = 'train/test')
    parser.add_argument('--num_thread', type = int,
                        default = 1, help = 'Number of threads')
    parser.add_argument('--result_list', type = str,
                        default = 'lipsync_face', help = 'Mention results list name')
    args=parser.parse_args()

    os.makedirs(args.name, exist_ok = True)

    args.download_dir = args.name.split('/')[-1]     
    args.download_dir=os.path.join(args.name, args.download_dir)
    args.sample_dir=os.path.join(args.name, args.sample_dir)
    args.cleaned_dir=os.path.join(args.name, args.cleaned_dir)
    args.result_list=os.path.join(args.name, args.result_list)

    json_dict={
        'project_meta': {
                'project_name': args.name, 'v_fps': args.v_fps, 'a_fps': args.a_fps, 'phase': args.phase, 'max_frames': args.max_frames
        },
        'video_data': [
               {'id': 0, 'path': "", 'duration': 0, 'isCleaned':0}
        ],
    }

    video_dict = {
                'video_id': 0, 'path': "", 'duration': 0, 'isCleaned':0,
                
                'scene':[ 
                        {'scene_id': 0, 'start': 0, 'end': 0,
                        'sample': [
                            {'sample_id': 0, 'start': 0, 'end': 0, 'isSync': 0, 'TalkID':0, 'isCleaned':0,
                            'frames': [{'frame_num': 0, 'face_id': 0,'bbox': [],'bsi':0}]
                            }     ]   
                        }
                    ]
                
        }

    lipsync_data_maker = Lipsync_datamaker(args, json_dict, video_dict)

    # Make a menu driven
    print("\nPrinting Args: ", args)
    print()

    print("\nAnnotate/Clean samples in %s\n" % args.download_dir)

    lipsync_data_maker.clean_samples()
