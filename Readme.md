# LipSync Annotator

This repository contains the code necessary to annotate already generated samples of a video

## Steps to follow:

1. Clone this repository using below command

```
git clone https://[username]@bitbucket.org/lipsync-2/lipsync_annotator.git```

```
2. Change directory to source

```
cd lipsync_annotator
```

3. Create python virtual environment (Python > 3.5 recommended)

```
virtualenv env
```

4. Launch virtual environment

```
source env/bin/activate
```

5. Install requirements

```
pip install -r requirements.txt
```

6. Annotate using below command. (Must provide --name parameter to target the sample folder)

```
python lipsync_annotator.py --name /project_folder/video_folder/
```
